# 1. Пользователь открывает консоль
# 2. Открывается начальная директория
# 3. с помощью cd можно перемещаться по директориям
# для этого нужен класс, который будет хранить состояние текущей директории
# + метод, обрабатывающий запрос пользователя
#   парсит строку и возвращает название функций
#   и приводит параметры в унифицированный вид
#   Стоит подумать о регулярке. Универсальной или уникальной для каждого метода? Хмммм

import os
import re
import console_command as cc


class CommandLine:
    def __init__(self):
        self.cwd = os.getcwd()
        self.filenames = []
        self.folders = []

    def change_directory(self, new_directory):
        """Меняет директорию"""
        path = self.join_path(self.cwd, new_directory)
        if os.path.isdir(path):
            self.cwd = path
        else:
            """Выбрасывать ошибку"""
            pass

    @staticmethod
    def print_new_line():
        """Выводит новую строку с текущей директорией"""
        print(command_line.cwd, end="> ")

    @staticmethod
    def join_path(cwd, name):
        return os.path.abspath(os.path.join(cwd, name))

    def input_command(self):
        return self.parse_command(input())

    @staticmethod
    def parse_parameters_part(parameters):
        options = {'options': '', 'simple_strings': []}
        for group in re.findall(r"(-\w+(?:\s\w\b)?)|([\w*?.]+)?", parameters):
            options['options'] += group[0]
            options['simple_strings'].append(group[1])
        return options

    def parse_command(self, command):
        if not command:
            return None, None

        command = command.strip().split(maxsplit=1)
        if len(command) == 2:
            name, string_parameters = command
        else:
            name, string_parameters = command[0], ""

        name = name.lower()
        if name == "cd":
            if string_parameters:
                return self.change_directory, {"new_directory": string_parameters}
            return None, {}
        elif name == "ls":
            if string_parameters:
                dict_parameters = self.parse_parameters_part(string_parameters)
                options = {
                    'options': dict_parameters['options'],
                    'pattern': dict_parameters['simple_strings'][0],
                }
                return self.ls, options
            return self.ls, {}
        elif name == "mkdir":
            if string_parameters:
                dict_parameters = self.parse_parameters_part(string_parameters)
                options = {
                    'options': dict_parameters['options'],
                    'directory': dict_parameters['simple_strings'][0],
                }
                return self.mkdir, options
            return None, {}
        elif name == "find":
            if string_parameters:
                dict_parameters = self.parse_parameters_part(string_parameters)
                options = {
                    'options': dict_parameters['options'],
                    'pattern': dict_parameters['simple_strings'][0],
                }
                return self.find, options
            return None, {}
        elif name == "del":
            if string_parameters:
                dict_parameters = self.parse_parameters_part(string_parameters)
                return self.delete_file, {"pattern": dict_parameters['simple_strings'][0]}
            return None, {}

    def ls(self, **kwargs):
        cc.ls(self.cwd, **kwargs)

    def mkdir(self, **kwargs):
        cc.mkdir(self.cwd, **kwargs)

    def find(self, **kwargs):
        cc.find(self.cwd, **kwargs)

    def delete_file(self, **kwargs):
        cc.delete_file(self.cwd, **kwargs)


if __name__ == "__main__":
    command_line = CommandLine()
    print("Драсте")
    while True:
        command_line.print_new_line()
        func, option = command_line.input_command()
        if func:
            print(option)
            func(**option)
