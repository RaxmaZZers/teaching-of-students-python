import os
import re
from time import gmtime, strftime


def list_files(cwd=os.getcwd()):
    """Возвращает список папок и файлов в директории"""
    return next(os.walk(cwd))[1:]


def print_files(files, end=' '):
    """Печатает список файлов"""
    for file in files:
        print(file, end=end)


def join_path(cwd, name):
    return os.path.abspath(os.path.join(cwd, name))


def get_files_size(cwd, files):
    """Возвращает список кортежей с именем файла и его размеров в байтах"""
    return [(file, os.path.getsize(join_path(cwd, file))) for file in files]


def sort_files_by_name(files):
    """Сортирует список файлов по имени"""
    return sorted(files, reverse=True)


def sort_files_by_size(cwd, files):
    """Сортирует список файлов по их размеру"""
    return sorted(files, key=lambda file: os.path.getsize(join_path(cwd, file)))


def sort_by_extension(files):
    """Сортирует список файлов по расширению"""
    # сортирую строку в обратном порядке, ведь точек может быть много, а то и вовсе не быть
    # собираю в строку и снова делю, первый элемент и будет именем расширения
    return sorted(files, key=lambda x: ''.join(sorted(x, reverse=True)).split('.')[0])


def sort_by_name(files, reverse=True):
    """Соритрует список файлов по имени"""
    return sorted(files, reverse=reverse)


def filtered_by_pattern(pattern, files):
    """Возвращает список файлов, которые подходят под шаблон 'pattern'"""
    pattern = re.sub(r'\.\*|\*', '.*', pattern)  # замена * или .* на .*
    pattern = re.sub(r'\.\?|\?', '.?', pattern)  # замена ? или .? на .?
    pattern = re.compile(pattern + '$')
    return [file for file in files if pattern.match(file)]


def ls(cwd=os.getcwd(), options: str = '', pattern: str = ''):
    # получение списка папок и файлов
    folders, files = list_files(cwd)

    if pattern:
        # фильтрация по шаблону
        files = filtered_by_pattern(pattern, files)
        folders = filtered_by_pattern(pattern, folders)

    if 'r' in options:
        # сортировка по имени
        files = sort_by_name(files)
        folders = sort_by_name(folders)

    if 'X' in options:
        # сортировка по расширению
        files = sort_by_extension(files)

    if 'S' in options:
        files = sort_files_by_size(cwd, files)

    if 's' in options:
        files = get_files_size(cwd, files)

    print(f' Содержимое папки {cwd}\n')
    print_files(files, end='\n')
    print_files(folders, end='\n')


def find(cwd: str, pattern: str, options: str = ''):
    filtered_files = []
    filtered_folders = []
    for root, dirs, files in os.walk(cwd):
        root = root.replace(cwd, '.')
        if pattern:
            # фильтрация по шаблону
            if 'type f' in options:
                filtered_files += [root + '\\' + file for file in filtered_by_pattern(pattern, files)]
            elif 'type d' in options:
                filtered_folders += [root + '\\' + dir_ for dir_ in filtered_by_pattern(pattern, dirs)]
            else:
                filtered_files += [root + '\\' + file for file in filtered_by_pattern(pattern, files)]
                filtered_folders += [root + '\\' + dir_ for dir_ in filtered_by_pattern(pattern, dirs)]

    if 'r' in options:
        # сортировка по имени
        filtered_files = sort_by_name(filtered_files)
        filtered_folders = sort_by_name(filtered_folders)

    if 's' in options:
        filtered_files = get_files_size(cwd, filtered_files)

    if filtered_files:
        print(*filtered_files, sep='\n')
    if filtered_folders:
        print(*filtered_folders, sep='\n')


def mkdir(cwd: str, directory: str, options: tuple = ()):
    if 'now' in options:
        directory += strftime("%d-%m-%Y", gmtime())
    if dir:
        os.mkdir(join_path(cwd, directory))


def cat(cwd, filename, text=''):
    with open(join_path(cwd, filename), 'w') as f:
        f.write(text)


def delete_file(cwd, pattern: str):
    filtered_files = []
    filtered_folders = []
    for root, dirs, files in os.walk(cwd):
        # root = root.replace(cwd, '.')
        if pattern:
            # фильтрация по шаблону
            filtered_files += [root + '\\' + file for file in filtered_by_pattern(pattern, files)]
            filtered_folders += [root + '\\' + dir_ for dir_ in filtered_by_pattern(pattern, dirs)]
    for file in filtered_files:
        print(file)
        os.remove(file)
    for dir_ in filtered_folders:
        print(dir_)
        # удалить папку


def exec_commands(cwd, filename):
    pass


if __name__ == '__main__':
    path = r'C:\Users\irahm\Documents\GitHub\teaching-of-students-python\Меджидов\Copy Linux console'
    find(path, pattern='lab*')
