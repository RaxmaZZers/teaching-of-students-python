import unittest
import os
from shutil import rmtree
from random import shuffle
import console_command as cc


class LSTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.original_dir = os.getcwd()

        os.mkdir('test')
        os.chdir(f"{os.getcwd()}\\test")
        cls.test_dir = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'test')

        cls.expected_files = [
            'textfile1.txt',
            'textfile2.txt',
            'script1.py',
            'script2.py',
            'img1.png',
            'img2.png',
            'img3.png',
            'img1.jpeg',
            'img2.jpeg',
            'img3.jpeg',
            'img4.jpg',
        ]

        for filename in cls.expected_files:
            with open(filename, 'w'):
                pass

    @classmethod
    def tearDownClass(cls):
        os.chdir(cls.original_dir)
        path = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'test')
        rmtree(path)

    def test_list_files(self):
        expected_folders = []
        actual_folders, actual_files = cc.list_files(self.test_dir)

        self.expected_files.sort()
        actual_files.sort()
        expected_folders.sort()
        actual_folders.sort()

        self.assertListEqual(actual_folders, expected_folders)
        self.assertListEqual(actual_files, self.expected_files)

    def test_sort_by_name(self):
        shuffle(self.expected_files)
        self.assertListEqual(sorted(self.expected_files), cc.sort_by_name(self.expected_files))
        shuffle(self.expected_files)
        self.assertListEqual(sorted(self.expected_files, reverse=True),
                             cc.sort_by_name(self.expected_files, reverse=True))


if __name__ == '__main__':
    unittest.main()
